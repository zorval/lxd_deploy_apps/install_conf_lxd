#!/bin/bash

# TODO
# - logrotate (all CT)
# - iptables isolateur: Deny in !80 !443
# - iptables isolateur: Deny out !80 !443 from rvprx
# - iptables isolateur: Deny out !25 from smtp

################################################################################
# BSD 3-Clause License
# 
# Copyright (c) 2018-2019, Alban Vidal <alban.vidal@zordhak.fr>
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
# 
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
# 
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
################################################################################

################################################################################
##########                    Define color to output:                 ########## 
################################################################################
_WHITE_="tput sgr0"
_RED_="tput setaf 1"
_GREEN_="tput setaf 2"
_ORANGE_="tput setaf 3"
_BLUE_="tput setaf 4"
################################################################################

# Path of git repository
# ./
GIT_PATH="$(realpath ${0%/*})"

################################################################################

# If vars file exist, source
if [ -f $GIT_PATH/config/00_VARS ]; then
    source $GIT_PATH/config/00_VARS
else
    echo
    echo "$($_BLUE_)File « $GIT_PATH/config/00_VARS » don't exist$($_WHITE_)"
    echo
    echo "$($_BLUE_)Please answer the following questions$($_WHITE_)"

    ##### BEGIN STORAGE SELECTION
    echo
    echo "$($_BLUE_)Please select the type of storage you want to use$($_WHITE_)"

    STORAGE_TYPE_LIST="loop block"

    PS3="Your choice: "

    # TODO select pour la taille du loop

    #select STORAGE_TYPE in $STORAGE_TYPE_LIST
    select STORAGE_TYPE in "lvm loop" "lvm block" "btrfs loop (very slow)" "btrfs block (slow)"
    do
        case $STORAGE_TYPE in
            "lvm loop"* )
                echo "$($_ORANGE_)You are select lvm loop$($_WHITE_)"
# Time: 1m31.501s
LXD_DEFAULT_STORAGE='
storage_pools:
- config:
    size: 20GB
  description: ""
  name: default
  driver: lvm
'
                break
                ;;
            "lvm block"* )
                echo "$($_ORANGE_)You have selected lvm block$($_WHITE_)"
                echo "$($_BLUE_)Now, please, enter path of block device (example /dev/sdb):$($_WHITE_)"
                read BLOCK_DEVICE
                if [ -b "$BLOCK_DEVICE" ] ; then
# Time: 1m22.096s
LXD_DEFAULT_STORAGE="
storage_pools:
- config:
    source: $BLOCK_DEVICE
  description: ''
  name: default
  driver: lvm
"
                else
                    echo "$($_RED_)ERROR, $BLOCK_DEVICE is not a block device$($_WHITE_)"
                    echo "EXIT"
                    exit 89
                fi
                break
                ;;
            "btrfs loop"* )
                echo "$($_ORANGE_)You have selected btrfs loop$($_WHITE_)"
# Time: 4m0.135s
LXD_DEFAULT_STORAGE='
storage_pools:
- config:
    size: 20GB
  description: ""
  name: default
  driver: btrfs
'
                break
                ;;
            "btrfs block"* )
                echo "$($_ORANGE_)You have selected btrfs block$($_WHITE_)"
                echo "$($_BLUE_)Now, please, enter path of block device (example /dev/sdb):$($_WHITE_)"
                read BLOCK_DEVICE
                if [ -b "$BLOCK_DEVICE" ] ; then
# Time: 2m43.360s
LXD_DEFAULT_STORAGE="
storage_pools:
- config:
    source: $BLOCK_DEVICE
  description: ''
  name: default
  driver: btrfs
"
                else
                    echo "$($_RED_)ERROR, $BLOCK_DEVICE is not a block device$($_WHITE_)"
                    echo "EXIT"
                    exit 90
                fi
                break
                ;;
            * )
                echo "$($_RED_)WARNING, please select correct answer$($_WHITE_)"
        esac
    done

    ##### END STORAGE SELECTION

    echo
    echo "$($_ORANGE_)** TECHNICAL **$($_WHITE_)"
    echo

    # Network interface
    echo
    ip -br -c a
    echo
    echo -n "$($_BLUE_)Internet network interface ('ip -br -c a' command is used to help you): $($_WHITE_)"
    read INTERNET_ETH
    # Test if interface exist
    ip a s $INTERNET_ETH > /dev/null 2>&1
    if [ $? -ne 0 ] ; then
        echo "$($_RED_)ERROR$($_WHITE_)"
        echo "$($_RED_)$INTERNET_ETH is not a valid interface$($_WHITE_)"
        echo "EXIT"
        exit 88
    fi

    # FQDN of baremetal server
    echo
    echo -n "$($_BLUE_)FQDN of server:$($_WHITE_) "
    read FQDN
    # Email for technical alert
    echo -n "$($_BLUE_)Technical Administrator Email of server:$($_WHITE_) "
    read TECH_ADMIN_EMAIL

    cat << EOF > $GIT_PATH/config/00_VARS
#
# Backend storage
LXD_DEFAULT_STORAGE="$LXD_DEFAULT_STORAGE"
#
# 
INTERNET_ETH="$INTERNET_ETH"
#
# FQDN host
FQDN="$FQDN"
#
# 
TECH_ADMIN_EMAIL="$TECH_ADMIN_EMAIL"
EOF

    echo ""
    echo "$($_ORANGE_)File « $GIT_PATH/config/00_VARS » generated$($_WHITE_)"
    echo ""

fi

# Load Network Vars
source $GIT_PATH/config/01_NETWORK_VARS

# Load Other vars 
# - DEBIAN_RELEASE
source $GIT_PATH/config/03_OTHER_VARS

################################################################################
#### HOST CONFIGURATION

#############
echo "$($_ORANGE_)Update repository and Upgrade system packages and default apt configuration$($_WHITE_)"

PACKAGES="vim apt-utils bsd-mailx unattended-upgrades apt-listchanges bind9-host logrotate postfix git"

if [ "$DEBIAN_RELEASE" == "stretch" ] ; then
    # Add backports
    echo 'deb http://ftp.fr.debian.org/debian stretch-backports main' > /etc/apt/sources.list.d/stretch-backports.list
fi


apt-get update > /dev/null
DEBIAN_FRONTEND=noninteractive apt-get -y install $PACKAGES > /dev/null
DEBIAN_FRONTEND=noninteractive apt-get -y upgrade > /dev/null

# Basic Debian configuration
mkdir -p /srv/git
git clone https://framagit.org/zorval/config_system/basic_config_debian.git /srv/git/basic_config_debian
# Setup config file for auto configuration
                                            >  /srv/git/basic_config_debian/conf
echo "UNATTENDED_EMAIL='$TECH_ADMIN_EMAIL'" >> /srv/git/basic_config_debian/conf
echo "GIT_USERNAME='$HOSTNAME'"             >> /srv/git/basic_config_debian/conf
echo "GIT_EMAIL='root@$HOSTNAME'"           >> /srv/git/basic_config_debian/conf
echo "SSH_EMAIL_ALERT='$TECH_ADMIN_EMAIL'"  >> /srv/git/basic_config_debian/conf
# Launch auto configuration script
/srv/git/basic_config_debian/auto_config.sh

#############

# TODO
# NE PAS FAIRE ICI, mais par projet ??
# Utile ??
#
#echo "$($_ORANGE_)Test if FQDN records A and PTR is OK$($_WHITE_)"
#TEST_IP=$(host -t A $FQDN|awk '{print $4}')
#TEST_FQDN=$(host -t PTR $TEST_IP|awk '{print $5}')
## Remove « . » in end on PTR record
#if [ "${TEST_FQDN::-1}" != "$FQDN" ] ; then
#    echo "$($_RED_)"
#    echo "ERROR DNS RECORDS"
#    echo "Your FQDN « $FQDN » is not equal to PTR value: « $TEST_FQDN »"
#    echo "Please fix that and retry"
#    echo "$($_WHITE_)"
#    exit 1
#else
#    echo "$($_GREEN_)FQDN records A and PTR is OK$($_WHITE_)"
#fi

#############

echo "$($_ORANGE_)Edit postfix configuration file (in server)$($_WHITE_)"
# Save old conf file
mv /etc/postfix/main.cf /etc/postfix/main.cf.OLD_$(date +%F)
# Copy postfix template and edit it
cp $GIT_PATH/templates/all/etc/postfix/main.cf /etc/postfix/main.cf
sed -i                                      \
    -e "s/__FQDN__/$FQDN/"                  \
    -e "s/__IP_smtp_PRIV__/$IP_smtp_PRIV/"  \
    /etc/postfix/main.cf
systemctl restart postfix

# Send crontab return to admin (TECH_ADMIN_EMAIL)
sed -i "1s/^/# Send cron report to admin\nMAILTO='$TECH_ADMIN_EMAIL'\n\n/" /etc/crontab

# Nat post 80 and 443 => RVPRX
# Enable Masquerade and NAT rules
echo "$($_ORANGE_)Install: iptables-persistent$($_WHITE_)"
DEBIAN_FRONTEND=noninteractive apt-get -y install iptables-persistent > /dev/null
echo "$($_ORANGE_)Enable Masquerade and NAT rules$($_WHITE_)"
cat << EOF > /etc/iptables/rules.v4
################################################################################
##########                          TABLE FILTER                      ########## 
################################################################################
## TODO
#*filter
#:INPUT ACCEPT [0:0]
#:FORWARD ACCEPT [0:0]
#:OUTPUT ACCEPT [0:0]
## Deny SMTP output if source is not smtp server
#-A OUTPUT -s ${IP_smtp}/32 -o lxdbrEXT -p tcp -m tcp --dport 25 -j ACCEPT -m comment --comment "Allow send mail (SMTP) FROM smtp container"
#-A OUTPUT -o lxdbrEXT -p tcp -m tcp --dport 25 -j DROP -m comment --comment "Deny SMTP if is not from SMTP container"
#COMMIT
################################################################################
##########                          TABLE NAT                         ########## 
################################################################################
*nat
####
:PREROUTING ACCEPT [0:0]
# Internet Input (PREROUTING)
-N zone_wan_PREROUTING
-A PREROUTING -i $INTERNET_ETH -j zone_wan_PREROUTING -m comment --comment "Internet Input PREROUTING"
# NAT 80 > RVPRX (nginx)
-A zone_wan_PREROUTING -p tcp -m tcp --dport 80 -j DNAT --to-destination $IP_rvprx:80 -m comment --comment "Routing port 80 > RVPRX - TCP"
# NAT 443 > RVPRX (nginx)
-A zone_wan_PREROUTING -p tcp -m tcp --dport 443 -j DNAT --to-destination $IP_rvprx:443 -m comment --comment "Routing port 443 > RVPRX - TCP"
COMMIT
EOF
iptables-restore /etc/iptables/rules.v4

##### DEBIAN
echo "$($_ORANGE_)Install: snapd, udev, btrfs and lvm$($_WHITE_)"
DEBIAN_FRONTEND=noninteractive apt-get -y install snapd udev btrfs-tools lvm2 thin-provisioning-tools > /dev/null
DEBIAN_FRONTEND=noninteractive apt-get clean

echo "$($_ORANGE_)Install: LXD with snap$($_WHITE_)"
if ! snap install lxd --channel="$LXD_VERSION" ; then
    echo "Error, installation LXD failed"
    echo "Please check logs and try again"
    echo "If the problem persists, please open an issue on framagit"
    exit 99
fi

##### UBUNTU
## Install LXD package
#apt-get install lxd-client/trusty-backports
#apt-get install lxd/trusty-backports
##apt-get install lxd

echo "$($_GREEN_)LXD is installed$($_WHITE_)"
echo ""
echo "$($_RED_)Please logout/login in bash to prevent snap bug and start script :$($_WHITE_)"
echo "$($_GREEN_)11_install_next.sh$($_WHITE_)"

# Test if /run/reboot-required file exist, and print warning
if [ -f /run/reboot-required ] ; then
    echo "$($_RED_)!! WARNING !!$($_WHITE_)"
    echo "$($_RED_)/run/reboot-required exist, you need to reboot this node before next step$($_WHITE_)"
fi
