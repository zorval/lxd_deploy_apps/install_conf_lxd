#!/bin/bash

# BSD 3-Clause License
# 
# Copyright (c) 2018, Alban Vidal <alban.vidal@zordhak.fr>
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
# 
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
# 
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

################################################################################
##########                    Define color to output:                 ########## 
################################################################################
_WHITE_="tput sgr0"
_RED_="tput setaf 1"
_GREEN_="tput setaf 2"
_ORANGE_="tput setaf 3"
################################################################################

# Path of git repository
# ../
GIT_PATH="$(realpath ${0%/*/*})"

# Load Vars
source $GIT_PATH/config/00_VARS

# Load Network Vars
source $GIT_PATH/config/01_NETWORK_VARS

# Load Resources Vars
source $GIT_PATH/config/02_RESOURCES_VARS

################################################################################
# Define name of container
CT_NAME="smtp"
################################################################################

#### SMTP
echo "$($_GREEN_)BEGIN smtp$($_WHITE_)"
echo "$($_ORANGE_)Install specific packages$($_WHITE_)"
lxc exec ${CT_NAME} -- bash -c "DEBIAN_FRONTEND=noninteractive apt-get -y install postfix > /dev/null"

echo "$($_ORANGE_)Configure postfix (/etc/postfix/main.cf)$($_WHITE_)"
lxc file push $GIT_PATH/templates/smtp/etc/postfix/main.cf ${CT_NAME}/etc/postfix/main.cf
lxc exec ${CT_NAME} -- bash -c "
    sed -i                                                  \
            -e 's#__FQDN__#$FQDN#'                          \
            -e 's#__PRIVATE_NETWORK__#$PRIVATE_NETWORK#'    \
            -e 's#__IP_smtp_PRIV__#$IP_smtp_PRIV#'          \
            /etc/postfix/main.cf
"
lxc exec ${CT_NAME} -- bash -c "
    echo $FQDN > /etc/mailname
    systemctl restart postfix
    echo Test SMTP $FQDN|mail -s 'Test SMTP $FQDN' -- $TECH_ADMIN_EMAIL
"

################################################################################

echo "$($_ORANGE_)Clean packages cache (.deb files)$($_WHITE_)"
lxc exec ${CT_NAME} -- bash -c "
    apt-get clean
"

echo "$($_ORANGE_)Reboot container to free memory$($_WHITE_)"
lxc restart ${CT_NAME} --force

echo "$($_ORANGE_)Set CPU and Memory limits$($_WHITE_)"
lxc profile add ${CT_NAME} $LXC_PROFILE_smtp_CPU
lxc profile add ${CT_NAME} $LXC_PROFILE_smtp_MEM

echo "$($_GREEN_)END smtp$($_WHITE_)"
echo ""

